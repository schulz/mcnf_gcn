module MCNFs

    using Graphs
    using StatsBase
    using CSV
    using DataFrames
    
    export MCNF, random_mcnf, load_mcnf, read_csv
    
    struct MCNF
        # MCNF instance container
        G::SimpleDiGraph{Int64}
        costs::Dict{Tuple{Int64,Int64}, Float64}
        capacities::Dict{Tuple{Int64, Int64}, Float64}
        demands::Array{Float64, 2}
    end
    
    function random_demands(graph::SimpleDiGraph{Int64}, n_demands::Int64, volumes::Array{Int64})
        # Generates a set of demands: samples origins and destinations from a uniform distribution and volume from the list of values in `volumes`
        nodes = collect(vertices(graph))
        demands = Array{Int64, 2}(undef, n_demands, 3)
        for i in 1:n_demands
            demands[i, 1:2] = sample(nodes, 2, replace=false)
            demands[i,3] = sample(volumes)
        end 
        return demands
    end
    
    function random_mcnf(graph::SimpleDiGraph{Int64}, n_demands::Int64, volumes::Array{Int64})
        # Generates a random MCNF instance defined over the graph `graph` and with `n_demands` demands.
        # cost function
        costs = Dict{Tuple{Int64, Int64}, Float64}()
        capacities = Dict{Tuple{Int64, Int64}, Float64}()
        for v in vertices(graph)
            costs[v] = 1.
            capacities[v] = 1.
        end
        demands = random_demands(graph, n_demands, volumes)
        return MCNF(graph, costs, capacities, demands)
    end
    
    function read_csv(csv_file_path::String)
        # read the contents of a CSV file, strip spaces from column names and set them to lowercase
        # returns a DataFrame object
        df = DataFrame(CSV.File(csv_file_path))
        for col in names(df)
            rename!(df, col => lowercase(strip(col)))
        end
        return df
    end
    
    function load_graph(link_file_path::String)
        # Load graph data
        # read the link data
        link_data = read_csv(link_file_path)
        # count the number of vertices
        n_vertices = 1+maximum([maximum(link_data.srcnodeid),maximum(link_data.dstnodeid)])
        G = SimpleDiGraph(n_vertices)
        costs = Dict{Tuple{Int64,Int64}, Float64}()
        capacities = Dict{Tuple{Int64,Int64}, Float64}()
        for row in eachrow(link_data)
            edge = (row.srcnodeid+1, row.dstnodeid+1)
            add_edge!(G, edge[1], edge[2])
            add_edge!(G, edge[2], edge[1])
            costs[edge] = row.cost
            costs[reverse(edge)] = row.cost
            capacities[edge] = row.bandwidth
            capacities[reverse(edge)] = row.bandwidth
        end
        return G, costs, capacities
        
    end
    
    function load_demands(demand_file_path::String)
        # Load demand data from a csv file
        demand_data = read_csv(demand_file_path)
        x = demand_data[!,["srcnodeid","dstnodeid","bandwidth"]]
        return Array{Float64, 2}(x)
    end
    
    function load_mcnf(link_file_path::String, demand_file_path::String)
        # Load MCNF insance defined by a link file at `link_file_path` and a list of demands at `demand_file_path`
        G, costs, capacities = load_graph(link_file_path)
        demands = load_demands(demand_file_path)
        return MCNF(G, costs, capacities, demands)
    end

    function load_mcnf(instance_path::String)
        return load_mcnf(joinpath(instance_path, "link.csv"), joinpath(instance_path, "service.csv"))
    end

end
