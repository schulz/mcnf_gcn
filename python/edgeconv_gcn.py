import sys
import numpy as np

import torch
import torch.nn.functional as F
from torch.nn import BatchNorm1d as BN
from torch.nn import Sequential, Linear, ReLU, ELU
from torch_geometric.nn import MessagePassing
from torch.nn import Linear, Parameter
from torch_geometric.utils import add_self_loops, degree

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class SimpleBipartiteLayer(MessagePassing):
    def __init__(self, edge_dim, dim, aggr):
        super(SimpleBipartiteLayer, self).__init__(aggr=aggr, flow="source_to_target")
        #self.nn = Sequential(Linear(3 * dim, dim), ReLU(), Linear(dim, dim), ReLU(),
        #                     BN(dim))


        #self.nn = Sequential(Linear(2 * dim + dim, dim), ReLU(), Linear(dim, dim), ReLU(),
        #                     BN(dim))
        self.nn = Sequential(Linear(3 * dim , dim), ELU(), Linear(dim, dim), ELU(),
                             BN(dim))



        # Maps edge features to the same number of components as node features.
        self.edge_encoder = Sequential(Linear(edge_dim, dim), ELU(), Linear(dim, dim), ELU(),
                                       BN(dim))

    def forward_bak(self, source, target, edge_index, edge_attr, size):
        # Map edge features to embeddings with the same number of components as node embeddings.
        edge_embedding = self.edge_encoder(edge_attr)

        out = self.propagate(edge_index, x=source, t=target, edge_attr=edge_embedding, size=size)

        return out

#    def forward(self, source, edge_index, edge_attr, size):
    def forward(self, x, edge_index, edge_features):
        # Map edge features to embeddings with the same number of components as node embeddings.
        edge_embedding = self.edge_encoder(edge_features)

        #out = self.propagate(edge_index, x=source,  edge_attr=edge_embedding, size=size)
        out = self.propagate(edge_index, x=x,  edge_attr=edge_embedding)

        return out



    def message_bak(self, x_j, t_i, edge_attr):
        return self.nn(torch.cat([t_i, x_j, edge_attr], dim=-1))
    def message(self, x_i, x_j, edge_attr):
        a= torch.cat([x_i, x_j, edge_attr], dim=-1)
        r = self.nn(a)
        return r


    def __repr__(self):
        return '{}(nn={})'.format(self.__class__.__name__, self.nn)


class EdgeConv(MessagePassing):
    def __init__(self, in_channels, out_channels, edge_channels, aggr="max"):
        super().__init__(aggr=aggr) #  "Max" aggregation.
        self.mlp = Sequential(Linear(2 * in_channels + edge_channels, out_channels),
                       ReLU(),
                       Linear(out_channels, out_channels))

    def forward(self, x, edge_index, edge_features):
        # x has shape [N, in_channels]
        # edge_index has shape [2, E]

        return self.propagate(edge_index, x=x, edge_features=edge_features)

    #def message(self, x_i, x_j, edge_features):
    def message(self, x_i, edge_features):
        tmp = torch.cat([x_i, edge_features], dim=1)  # tmp has shape [E, 2 * in_channels]
        return self.mlp(tmp)

class EdgeConvSimpleNet(torch.nn.Module):
    _class_prefix = "ECS"
    def __init__(self, output_dim , hidden, aggr, num_layers,  node_dim=2, regression=False):
        super(EdgeConvSimpleNet, self).__init__()
        self.num_layers = num_layers

        self.regression = regression
        
        self.M = torch.randn((2*hidden*(num_layers+1),2*hidden*(num_layers+1)+1), requires_grad=True )
        self.N = torch.randn(2*hidden*(num_layers+1), requires_grad=True )
        self.O = torch.randn(2*hidden*(num_layers+1)+1, requires_grad=True )



        # Embed initial node features.
        self.node_encoder = Sequential(Linear(node_dim, hidden), ELU(), Linear(hidden, hidden))

        # Bipartite GNN architecture.
        self.layers = []
        for i in range(self.num_layers):
            self.layers.append(SimpleBipartiteLayer(2, hidden, aggr=aggr))
            #self.layers.append(EdgeConv(4, hidden, 2, aggr=aggr))

        self.layers = torch.nn.ModuleList(self.layers)

        # MLP used for classification.
        self.lin1 = Linear((num_layers + 1) * hidden, hidden)
        self.lin2 = Linear(hidden, hidden)
        self.lin3 = Linear(hidden, hidden)
        if not self.regression:
            self.lin4 = Linear(hidden, output_dim)
        else:
            self.lin4 = Linear(hidden, output_dim)
    def parameters(self):
        for p in super().parameters():
            yield p
        yield self.M
        yield self.N
        yield self.O

    def forward(self, data):

        # Get data of batch.
        node_features = data.node_features
        edge_index = data.edge_index
        edge_features = data.edge_features
        num_nodes = data.num_nodes
        # Get the demand data from batch

        # Compute initial node embeddings.
        ns = (data.node_features.shape[0], 3)
        node_features = torch.from_numpy(np.random.normal(size=ns)).to(torch.float)

        node_features_0 = self.node_encoder(node_features)

        x_var = [node_features_0]

        num = node_features_0.size(0)
        for i in range(self.num_layers):
            x_var.append(F.relu(self.layers[i](x_var[-1], edge_index, edge_features)))

        x = torch.cat(x_var[:], dim=-1)

        # encode the demands
        x_demands = []
        for k in range(data.demands.shape[0]):
            x_demands.append(torch.cat([x[data.demands[k,0]], x[data.demands[k,1]], torch.tensor([data.demands[k,2]])], dim=-1))
        #x_demands = np.array(x_demands)
        x_demands = torch.stack(x_demands)
        
        edge_encodings = [torch.cat([x[edge_index[0,l]], x[edge_index[1,l]]], dim=-1) for l in range(edge_index.shape[1])]
        edge_encodings = torch.stack(edge_encodings)

        # apply M
        tmp = torch.matmul(edge_encodings, self.M)
        tmp1 = tmp + torch.matmul(edge_encodings, self.N)[None,:].t()

      
        lots = [] 
        for i,n_k in enumerate(data.num_demands):
            p0 = int(torch.sum(data.num_demands[:i]))
            e0 = int(torch.sum(data.num_edges_[:i]))
            xk_demands = x_demands[p0:p0+n_k]
            xk_edges = tmp1[e0:e0+data.num_edges_[i]]
            xk_edges = xk_edges[data.var_edges]

            tmp_3 = torch.matmul(xk_edges, xk_demands.t())
 
            tmp_2 = tmp_3+ torch.matmul(xk_demands, self.O)
            lots.append(tmp_2)
        r = torch.stack(lots, dim=0)
        r = F.log_softmax(r, dim=-1) 

        return r



        p = []
        for l in range(edge_index.shape[1]):
            edge_demand_scores = []
            for k in range(len(x_demands)):
                # edge node features
                x_i = x[edge_index[0,l]]
                x_j = x[edge_index[1,l]]
                # edge features
                ef = torch.cat([x_i, x_j], dim=-1)
                # demands features
                df = x_demands[k]
                score = torch.matmul(torch.matmul(ef, self.M), df) + torch.dot(ef, self.N) + torch.dot(df, self.O)
                edge_demand_scores.append(score)
                #p.append(score) 
            edge_demand_scores = torch.stack(edge_demand_scores)
            edge_demand_scores = F.log_softmax(edge_demand_scores, dim=-1)
            p.append(edge_demand_scores)
        p = torch.stack(p)
        return p

    def __repr__(self):
        return self.__class__.__name__
