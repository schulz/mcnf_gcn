"""
Generating datasets of MCNF problems.

Each dataset is a collection of N MCNF instances : 
    - {0..N}.mps : the LP file containing the MCNF instance, loadable with CPLEX
    - {0..N}.sol : solution file
    - {0..N}_graph.pkl : graph file

Type 1 : constant graph G=(V,E), costs, capacities and number of demands |K|. Demands created by taking two nodes at random and a bandwidth taken at random from the full list of demands. Nodes have no features, edge features are [cost_ij, capacity_ij]. 

Type 2 : constant graph G=(V,E), costs, capacities and number of demands |K|. Demands created as in Type 1. A node demand_k is added to the graph for every demand as well as arcs (demand_k, o_k), (t_k, demand_k). Each demand node is given a feature equals to the demands bandwidth. 


"""
import time
import copy
import os
import network
import networkx as nx
import graph_to_mps
import numpy as np
from progress.bar import Bar
import matplotlib.pyplot as plt
from networkx.algorithms.components.weakly_connected import number_weakly_connected_components, weakly_connected_components

#import torch
#from torch_geometric.data import (InMemoryDataset, Data)
#from torch_geometric.data import DataLoader
#from torchmetrics import F1Score, Precision, Recall, Accuracy

#from dataset import GraphDataset, MyData, MyTransform, create_data_object
from graph_to_mps import plot_mcnf_instance, plot_mcnf_solution

from mcnf import MCNF


def get_demands(G, n):
    original_demands = np.array(network.load_full_demands())
    demands = []
    # only choose source and destination in layer 0 or 4
    nodes = [n for n in G.nodes if G.nodes[n]["layer"]==0 or G.nodes[n]["layer"]==4]
    for i in range(n):
        [source,dest] = np.random.choice(nodes, replace=False, size=2)
        bandwidth = np.random.choice(original_demands[:,2])
        demands.append([source,dest,bandwidth])
    r=np.array(demands)
    return r

def make_instance(output_dir, dataset_type=1):
    pass

def component_node_features(component, node, G, forward=True):
    """Compute the features given to the arc (component_node, node) if forward, (node, component) if not forward
    """
    features = []
    for c in component:
        if G.has_edge(node,c) and not forward:
            features.append([G.edges[node,c]["cost"], G.edges[node,c]["bandwidth"]])
        if G.has_edge(c,node) and forward:
            features.append([G.edges[c,node]["cost"], G.edges[c,node]["bandwidth"]])
    if len(features)==1:
        return features[0]
    return np.min(features, axis=0)

def component_node_is_extremity(node, component, G):
    #print("in compontnent extremities ")
    node_neighbors = list(G.predecessors(node)) + list(G.successors(node))
    node_neighbors = set(node_neighbors)
    flags = [n in component for n in node_neighbors]
    #print(node_neighbors)
    #print(flags)
    #print(all(flags))
    return not all(flags)

def get_component_extremities(component, G):
    c = []
    for node in component:
        if component_node_is_extremity(node, component, G):
            c.append(node)
    return c
def get_component_neighbors(component, G):
    extremities = get_component_extremities(component, G)
    c = []
    for extr in extremities:
        ns = list(G.predecessors(extr)) + list(G.successors(extr))
        c += [n for n in ns if not n in component]
    return list(set(c))
 
def contract_graph(G, demands, exclude_nodes=[]):
    """Constructs a new graph H that is defined as G in which all chains have been replaced by 
    a new node.
    """
    #print("is multigraph ", G.is_multigraph())
    # new node increment 
    new_node_inc = max(G.nodes) + 1
    # temporary graph with all excluded nodes removed
    tG = G.subgraph([n for n in G.nodes if not n in exclude_nodes])
    # find all nodes that have degree greater then 2 (4 if graph is directed)
    removable_nodes = [n for n in G.nodes if G.degree(n)>4] + exclude_nodes
    # subgraph with those nodes removed
    subG = G.subgraph([n for n in G.nodes if not n in removable_nodes])
    # mapping of the original nodes to new nodes
    node_map = {n:n for n in removable_nodes}
    components = list(weakly_connected_components(subG))
    #print("Number components : ", len(components))
    #print("Removable nodes : ", len(removable_nodes))

    cG = nx.MultiDiGraph(G.subgraph(removable_nodes))
    for component in components:
        component = list(component)
        extr = list(get_component_extremities(component, G))
        neighbs = list(get_component_neighbors(component, G))
        if len(neighbs)==1:
            # only 1 neighbor
            # remove the component from the graph and map its nodes to the only neighbor
            for n in component:
                node_map[n] = neighbs[0]
        else:
            #print("\tsingle extremity and 2 neighbors : ", component)
            chain = nx.Graph(G.subgraph(component + neighbs))
            print("CHAIN : ", chain, len(chain))
            if chain.has_edge(neighbs[0], neighbs[1]):
                chain.remove_edge(neighbs[0], neighbs[1])
            if chain.has_edge(neighbs[1], neighbs[0]):
                chain.remove_edge(neighbs[1], neighbs[0])
            # sum up costs on component
            cost_sum = sum([nd["cost"] for _,_,nd in chain.edges(data=True)]+[0.])
            # get min bandwidth
            bw_min = min([nd["bandwidth"] for _,_,nd in chain.edges(data=True)])
            # add edge 
            def node_sequence_to_edge_sequence(seq):
                a = []
                for i in range(1, len(seq)):
                    a.append((seq[i-1], seq[i], 0))
                return a
            direct_path = nx.algorithms.shortest_path(chain, source=neighbs[0], target=neighbs[1], weight="cost")
            direct_path = node_sequence_to_edge_sequence(direct_path)
            revers_path = nx.algorithms.shortest_path(chain, source=neighbs[1], target=neighbs[0], weight="cost")
            revers_path = node_sequence_to_edge_sequence(revers_path)

            cG.add_edge(neighbs[0], neighbs[1], cost=cost_sum, bandwidth=bw_min, type="chain", chain=direct_path)
            cG.add_edge(neighbs[1], neighbs[0], cost=cost_sum, bandwidth=bw_min, type="chain", chain=revers_path)
            for n in component:
                node_map[n] = (neighbs[0], neighbs[1])
            #print("\t", component, " -> ", (neighbs[0], neighbs[1]))
        continue


        if len(extr)==1:
            if len(neighbs)==1:
                # only 1 neighbor
                # remove the component from the graph and map its nodes to the only neighbor
                for n in component:
                    node_map[n] = neighbs[0]
                #print("\tsingle extremity and neighbor : ", component)
            else:
                #print("\tsingle extremity and 2 neighbors : ", component)
                chain = nx.Graph(tG.subgraph(component + neighbs))
                # sum up costs on component
                cost_sum = sum([nd["cost"] for _,_,nd in chain.edges(data=True)]+[0.])
                # get min bandwidth
                bw_min = min([nd["bandwidth"] for _,_,nd in chain.edges(data=True)])
                # add edge 
                cG.add_edge(neighbs[0], neighbs[1], cost=cost_sum, bandwidth=bw_min)
                cG.add_edge(neighbs[1], neighbs[0], cost=cost_sum, bandwidth=bw_min)
                for n in component:
                    node_map[n] = (neighbs[0], neighbs[1])
                #print("\t", component, " -> ", (neighbs[0], neighbs[1]))



        else:
            if len(neighbs)==1:
                # single neighbor
                for n in component:
                    node_map[n] = neighbs[0]
                #print("\tsingle neighbor : ", component)
            else:
                # chain subgraph undirected
                chain = nx.Graph(tG.subgraph(component + neighbs))
                #print(chain, len(chain.nodes), len(chain.edges))
                # sum up costs on component
                #print("\tcosts" , [nd["cost"] for _,_,nd in chain.edges(data=True)])
                #print("\tbws ", [nd["bandwidth"] for _,_,nd in chain.edges(data=True)])
                cost_sum = sum([nd["cost"] for _,_,nd in chain.edges(data=True)]+[0.])
                # get min bandwidth
                bw_min = min([nd["bandwidth"] for _,_,nd in chain.edges(data=True)])
                #print("\tEdge ", neighbs[:2], cost_sum, bw_min)
                # add edge 
                cG.add_edge(neighbs[0], neighbs[1], cost=cost_sum, bandwidth=bw_min)
                cG.add_edge(neighbs[1], neighbs[0], cost=cost_sum, bandwidth=bw_min)
                for n in component:
                    node_map[n] = (neighbs[0], neighbs[1])
                #print("\t", component, " -> ", (neighbs[0], neighbs[1]))



    cdemands = []
    for demand in demands:
        cdemands.append([node_map[demand[0]], node_map[demand[1]], demand[2]])
    cdemands = np.array(cdemands)
 
    return cG, cdemands, node_map


def reconstruct_solution(G, G_contract, demands, demands_contract, solution,  node_map):
    # contracted graph still contains demand nodes, remove them
    tGc = G_contract.subgraph([n for n in G_contract if G_contract.nodes[n].get("type","")!="demand"])
    # graph with no demand nodes
    G_nodemand = G.subgraph([n for n,nd in G.nodes(data=True) if nd.get("type","")!="demand"])
    # edge-demand-variable mapping
    edge_demand_var_map = {}
    for e in G_nodemand.edges:
        for k in range(len(demands)):
            edge_demand_var_map[e[0], e[1], k] = len(edge_demand_var_map)
 

    m = len(tGc.edges)
    n_k = len(demands_contract)
    n_edges = len(G_nodemand.edges)
    edges_contract = list(tGc.edges)
    print("in reconstruct_solution ", len(demands), n_edges, len(demands) * n_edges)
    reconstructed_solution = np.zeros(len(demands) * n_edges)
    demand_paths = []
    for k in range(len(demands_contract)):
        # get the path used for each demand
        # get the arcs used for each demand
        kpos = k + np.arange(m)*n_k
        used_edges = []
        for j,f in enumerate(solution[kpos]):
            if f != 0.:
                used_edges.append(edges_contract[j])
        # reconstruct the path
        path = [demands_contract[k][0]]
        while not path[-1] == demands_contract[k][1]:
            for e in used_edges:
                if e[0] == path[-1]:
                    path.append(e[1])
                    used_edges.remove(e)
        real_path = []
        for j in range(len(path)-1):
            if G.has_edge(node_map[path[j]], node_map[path[j+1]]):
                if len(real_path)==0 or real_path[-1]!=node_map[path[j]]:
                    real_path += [node_map[path[j]], node_map[path[j+1]]]
                else:
                    real_path += [node_map[path[j+1]]]

            else:
                sp = nx.algorithms.shortest_path(G_nodemand, source=node_map[path[j]], target=node_map[path[j+1]], weight="cost")
                if len(real_path)==0 or real_path[-1]!=sp[0]:
                    real_path += sp
                else:
                    real_path += sp[1:]
        demand_paths.append(real_path)
        for j in range(len(real_path)-1):
            reconstructed_solution[edge_demand_var_map[real_path[j], real_path[j+1], k]] = demands_contract[k,2]
            #reconstructed_solution[edge_demand_var_map[real_path[j], real_path[j+1], k]] = 1.
    print("in reconstruct_solution ", reconstructed_solution.shape)
    return reconstructed_solution, demand_paths


def make_dataset(n, graph, n_demands, output_dir=None, dataset_type=2):
    if output_dir is None:
        output_dir = f"dataset_1_2_n{n}_k{n_demands}_type{dataset_type}"
    if os.path.exists(output_dir):
        os.system(f"rm -rf {output_dir}/*")
    os.makedirs(output_dir, exist_ok=True)


    solve_times = []
    size_reduction = []
    metric_values = []

    n_nodes = len(graph.nodes)
    n_edges = len(graph.edges)

    with Bar(f"Solving with {n_demands} demands: ", max = n) as bar:
        i=0
        while i<n:
            # copy the graph
            G = nx.MultiDiGraph(graph)

            # clear all demand info
            nx.set_node_attributes(G, {n:[] for n in G.nodes}, "node_demands")
            demands = get_demands(G, n_demands)
            mcnf = MCNF(G, demands)
            # edge-demand-variable mapping
            edge_demand_var_map = {}
            for e in G.edges:
                for k in range(len(demands)):
                    edge_demand_var_map[e[0], e[1], k] = len(edge_demand_var_map)
  
            # vector of demand bandwidths
            demand_bandwidths = np.array([d[2] for d in demands])
            cpx = graph_to_mps.get_mcnf_solver(G, demands, cost="cost", capacity="bandwidth")
            t0 = cpx.get_time()
            cpx.solve()
            t0 = cpx.get_time() - t0
            status = cpx.solution.get_status_string()
            if status=="infeasible":
                pass
            else:
                # write the .mps file
                cpx.write(os.path.join(output_dir, f"{i}.mps"))
                # write the .sol file
                cpx.solution.write(os.path.join(output_dir, f"{i}.sol"))
                solution = np.array(cpx.solution.get_values())
                # make the networkx graph representing the problem
                demand_nodes = []
                origin_destination_nodes = []

                G_nodemand = nx.MultiDiGraph(G)
                if dataset_type == 2:
                    # add a node for every demand
                    for k,demand in enumerate(demands):
                        new_node = max(G.nodes) + 1
                        G.add_node(new_node, type= "demand")
                        G.add_edge(new_node, demand[0], cost= 0., bandwidth= demand[2])
                        G.add_edge(demand[1], new_node, cost= 0., bandwidth= -demand[2])
                        demand_nodes.append(new_node)
                        origin_destination_nodes += list(demand[:2])
                node_color = []
                for no in G.nodes:
                    if G.nodes[no].get("type","")=="demand":
                        node_color.append("red")
                    else:
                        node_color.append("#1f78b4")
                #f = plot_mcnf_solution(G, demands, cpx.solution.get_values())
                #plt.show()

                
                # graph contraction
                print("contraction graph")
                print("\tdemand nodes : ", demand_nodes)
                print("\torigin destinations : ", origin_destination_nodes)
                G_contract, demands_contract, node_map = contract_graph(G, demands, exclude_nodes=demand_nodes+origin_destination_nodes)
                # contracted graph still contains demand nodes, remove them
                tGc = G_contract.subgraph([n for n in G_contract if G_contract.nodes[n].get("type","")!="demand"])
                print("Contracted graph ")
                print("\tnodes: ", len(tGc.nodes))
                print("\tedges: ", len(tGc.edges))
                print(demands_contract)
                size_reduction.append([float(len(tGc.nodes))/n_nodes, float(len(tGc.edges))/n_edges])
                print("\tSize reduction : ", size_reduction[-1])

                f, ax = plt.subplots(1,2, figsize=(15,15))
                plot_mcnf_instance(G.subgraph([n for n,nd in G.nodes(data=True) if nd.get("type","")!="demand"]), demands, ax=ax[0])
                plot_mcnf_instance(tGc, demands_contract, ax=ax[1])
                contraction_dir = "contracted_img2"
                os.makedirs(contraction_dir, exist_ok=True)
                plt.savefig(os.path.join(contraction_dir, str(i)+".png"))

                #plt.show()
                #plt.close("all")


                # solve the contracted problem
                cpx_contract = graph_to_mps.get_mcnf_solver(tGc, demands_contract, cost="cost", capacity="bandwidth")
                t1 = cpx_contract.get_time()
                cpx_contract.solve()
                t1 = cpx_contract.get_time() - t1
                status = cpx_contract.solution.get_status_string()
                if status == "optimal":
                    solution_contract = np.array(cpx_contract.solution.get_values())
                    print("\tContracted problem solve status : ", cpx_contract.solution.get_status_string())
                    print("\tContracted problem solve time : ", t0)
                    print("\tContracted solution :")
                    print("\tsolution size : ,", solution_contract.shape)
                    #print("\t", solution_contract)
                    #print(demands)
                    #print(demands_contract)
                    #reconstructed_solution = reconstruct_solution(solution_contract)
                    m = len(tGc.edges)
                    n_k = len(demands_contract)
                    edges_contract = list(tGc.edges)
                    reconstructed_solution = np.zeros(len(demands) * n_edges)
                    for k in range(len(demands_contract)):
                        #print("demand ", demands_contract[k])
                        # get the path used for each demand
                        # get the arcs used for each demand
                        kpos = k + np.arange(m)*n_k
                        #print("\tkpos ", kpos, len(kpos))
                        #print("\tflow ", solution_contract[kpos])
                        #print(len(edges_contract), n_k)
                        #print("\t", [edges_contract[p] for p in k+np.arange(m)])
                        used_edges = []
                        for j,f in enumerate(solution_contract[kpos]):
                            if f != 0.:
                                used_edges.append(edges_contract[j])
                        #print("\tused edges : ", used_edges)
                        # reconstruct the path
                        path = [demands_contract[k][0]]
                        while not path[-1] == demands_contract[k][1]:
                            for e in used_edges:
                                if e[0] == path[-1]:
                                    path.append(e[1])
                                    used_edges.remove(e)
                        #print("\tpath : ", path)
                        #print("\toriginal demand : ", demands[k])
                        real_path = []
                        for j in range(len(path)-1):
                            #print("EDGE ", path[j], path[j+1], node_map[path[j]], node_map[path[j+1]])
                            #print("\t", G.has_edge(node_map[path[j]], node_map[path[j+1]]))
                            if G.has_edge(node_map[path[j]], node_map[path[j+1]]):
                                if len(real_path)==0 or real_path[-1]!=node_map[path[j]]:
                                    real_path += [node_map[path[j]], node_map[path[j+1]]]
                                else:
                                    real_path += [node_map[path[j+1]]]

                            else:
                                #print("Missing edge : ", node_map[path[j]], node_map[path[j+1]])
                                sp = nx.algorithms.shortest_path(G_nodemand, source=node_map[path[j]], target=node_map[path[j+1]])
                                #print(f"shortest {node_map[path[j]]}-{node_map[path[j+1]]} path : ", sp)
                                if len(real_path)==0 or real_path[-1]!=sp[0]:
                                    real_path += sp
                                else:
                                    real_path += sp[1:]
                        #print("REAL PATH : ", real_path)
                        #print(list(demand[:2]))
                        for j in range(len(real_path)-1):
                            reconstructed_solution[edge_demand_var_map[real_path[j], real_path[j+1], k]] = demands_contract[k,2]
                            #reconstructed_solution[edge_demand_var_map[real_path[j], real_path[j+1], k]] = 1.
                    #print("Reconstructed solution shape : ", reconstructed_solution.shape)
                    #print("Solution shape : ", solution.shape)
                    rrr,paths = reconstruct_solution(G, G_contract, demands, demands_contract, solution_contract, node_map)
                    cc = np.array(cpx.objective.get_linear())
                    #if np.any(solution != rrr):
                    #    print(solution)
                    #    print(rrr)
                    #    print(rrr.shape, reconstructed_solution.shape)
                    #    print(np.sum(rrr == reconstructed_solution) / rrr.size, 10*"B")
                    #    print(np.sum(rrr == solution) / rrr.size, 10*"C")
                    #    print("Real optimal objective :", np.dot(cc, solution))
                    #    print("Pred optimal objective :", np.dot(cc, rrr))
                    #    
                    #    
                    #    def chain_edges(c):
                    #        e = []
                    #        for i in range(1, len(c)):
                    #            e.append((c[i-1], c[i]))
                    #        return e

                    #    original_paths = mcnf.solution_paths(solution)
                    #    pred_paths = mcnf.solution_paths(rrr)
                    #    for k in range(len(demands)):
                    #        orig_edges = chain_edges(original_paths[k])
                    #        orig_costs = [G.edges[e]["cost"] for e in orig_edges]
                    #        pred_edges = chain_edges(pred_paths[k])
                    #        pred_costs = [G.edges[e]["cost"] for e in pred_edges]
                    #        print("demand ", demands[k])
                    #        print("\tOrig ", original_paths[k])
                    #        print("\torig costs: ", orig_costs)
                    #        print("\tPred ", pred_paths[k])
                    #        print("\tpred costs : ", pred_costs)
                    #        

                    #    #plt.figure()
                    #    #plt.subplot(1,2,1)
                    #    #plot_mcnf_instance(G, demands, ax=plt.gca())
                    #    #plt.subplot(1,2,2)
                    #    #plot_mcnf_instance(tGc, demands_contract, ax=plt.gca())
                    #    #plt.show()

         
                        #exit()

                    for h in demands_contract:
                        if h[0]==h[1]:
                            print("Demand h ", h)
                            rr = input("hahah")
                    labs = (solution > 0.).astype(int)
                    pred = (reconstructed_solution > 0.).astype(int)
                    print(np.sum(labs == reconstructed_solution) / labs.size)
                    from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
                    print("accuracy : ", accuracy_score(labs, pred))
                    print("precision : ", precision_score(labs, pred))
                    print("recall    : ", recall_score(labs, pred))
                    print("f1 score  : ", f1_score(labs, pred))
                    cc = np.array(cpx.objective.get_linear())
                    obj1 = np.dot(cc, solution)
                    obj2 = np.dot(cc, rrr)
                    if obj1!=obj2:
                        print("original demands ")
                        ##print(demands)
                        ##print("contracted demands ")
                        ##print(demands_contract)
                        #print("solution")
                        #print(solution.reshape((n_edges, n_k)).T)
                        #print("reconstructed")
                        #print(reconstructed_solution.reshape((n_edges,n_k)).T)
                        ##print("real path")
                        ##print(real_path)

                        #original_paths = mcnf.solution_paths(solution)
                        #pred_paths = mcnf.solution_paths(rrr)

                        #reshaped_solution = solution.reshape((n_edges, n_k))
                        #reshaped_predict = rrr.reshape((n_edges, n_k))
                        #
                        #for k in range(n_k):
                        #    if np.any(reshaped_solution[:,k]!=reshaped_predict[:,k]):
                        #        print("Demande ", k)
                        #        print("\t", demands[k])
                        #        print("\t", demands_contract[k])
                        #        print("\tgood path : ", original_paths[k])
                        #        print("\tpred path : ", pred_paths[k])

                        #
                        ## null demand 
                        #print("There is a null demand : ", np.any(np.sum(reshaped_predict, axis=0)==0.))

                        #print("Cplex paths : ")
                        #for k,demand in enumerate(demands):
                        #    print("\t", demand, original_paths[k], np.unique(reshaped_solution[:,k]))
                        #print("Pred paths : ")
                        #for k,demand in enumerate(demands):
                        #    print("\t", demand, pred_paths[k], np.unique(reshaped_predict[:,k]))
                        ##print("Contracted demands : ")
                        ##print(demands_contract)

                        #print(f"Cplex obj / contraction obj: {obj1} / {obj2}")
                        #infeas = np.abs(np.array(cpx.solution.infeasibility.linear_constraints(rrr.tolist())))
                        #print("solution is feasible : ", np.sum(infeas)==0.)
                        #rr = input("hahah")
                    else:
                        # add the labels to each edge and demands to every node
                        nx.set_node_attributes(G_contract, {n:[] for n in G_contract.nodes}, "node_demands")
                        for k,demand in enumerate(demands_contract):
                            G_contract.add_node(f"d{k}", type= "demand")
                            G_contract.add_edge(f"d{k}", demand[0], cost= 0., bandwidth= 0.)
                            G_contract.add_edge(demand[1], f"d{k}", cost= 0., bandwidth= 0.)
                        demand_features = {n: np.zeros(1) for n in G_contract.nodes}
                        for k,demand in enumerate(demands_contract):
                            demand_features[f"d{k}"] += demand[2]
                        nx.set_node_attributes(G_contract, demand_features, "demands")
                        edge_labels = {}
                        var_edges = [e for e in G_contract.edges if G_contract.nodes[e[0]].get("type","")!="demand" and G_contract.nodes[e[1]].get("type","")!="demand"]
                        for j,e in enumerate(var_edges):
                            if G_contract.nodes[e[0]].get("type","")=="demand" or G_contract.nodes[e[1]].get("type","")=="demand":
                                continue
                            sol = np.array(solution_contract[j*n_demands:(j+1)*n_demands])
                            edge_labels[e] = np.array(solution_contract[j*n_demands:(j+1)*n_demands])
                            s = np.sum(edge_labels[e])
                            if s!=0.:
                                edge_labels[e] /= s
                        nx.set_edge_attributes(G_contract, edge_labels, "label")
                        for demand in demands_contract:
                            G_contract.nodes[demand[0]]["node_demands"].append(demand)


 
 
                        graph_filename = os.path.join(output_dir, f"{i}_graph.pkl")
                        nx.write_gpickle(G_contract, graph_filename)
                    
                    metric_values.append([accuracy_score(labs, pred),
                                          precision_score(labs, pred),
                                          recall_score(labs, pred),
                                          f1_score(labs, pred),
                                          np.abs(obj1-obj2)/obj1
                    ])
                        
                else:
                    print("\tinfeasible")
                solve_times.append([t0, t1])

                i += 1
                bar.next()
    solve_times = np.array(solve_times)
    size_reduction = np.array(size_reduction)
    metric_values = np.array(metric_values)
    f,ax = plt.subplots(2,1)
    plt.suptitle("Solve time and instance size reduction after contraction")
    ax[0].boxplot([solve_times[:,0], solve_times[:,1]], labels=["original", "contracted"])
    ax[0].set_ylabel("Solve time (s)")
    ax[1].boxplot([size_reduction[:,0], size_reduction[:,1]], labels=["nodes", "edges"])
    ax[1].set_ylabel("Reduction (new_size/original_size)")

    plt.savefig("contraction.png")

    f, ax = plt.subplots(1, 1)
    ax.boxplot([metric_values[:,i] for i in range(metric_values.shape[1])], labels=["accuracy","precision","recall","f1","opt"])
    plt.savefig(os.path.join(output_dir, "metric_values.png"))
    
    
   

if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("n", help="number of samples to generate", type=int, default=1000)
    parser.add_argument("n_demands", help="number of demands per instance", type=int, default=10)


    args = parser.parse_args()
    print(args)

    # choose a graph
    G = network.get_subgraph(1, 1)
    print(G)
    
    make_dataset(args.n, G, args.n_demands, output_dir=f"dataset_subgraph_1_1_n{args.n}_k{args.n_demands}")


