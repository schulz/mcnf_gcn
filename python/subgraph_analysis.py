import network
from networkx.algorithms.components.weakly_connected import number_weakly_connected_components, weakly_connected_components
import os
import numpy as np
import matplotlib.pyplot as plt

image_directory = "docs/images"

x = []
chain_count = []
chain_length = []

for subgraph in network.iter_subgraph():
    print(subgraph)
    degrees = np.array([d for n, d in subgraph.degree()])
    x.append([len(subgraph.nodes), len(subgraph.edges), np.mean(degrees)])

    # nodes with degree greater then 
    nodes = [n for n in subgraph.nodes if subgraph.degree(n)<=4] 
    cG = subgraph.subgraph(nodes)
    components = list(weakly_connected_components(cG))
    chain_count.append(len(components))
    chain_length.append(np.mean([len(c) for c in components]))


x = np.array(x)


# plot distribution of subgraph node and edge counts and the mean degree
plt.figure(figsize=(15,5))
plt.suptitle("Subgraph analysis")
plt.subplot(1,2,1)
ec = plt.hist2d(np.log(x[:,0]), np.log(x[:,1]), bins=(50,50), cmap=plt.cm.jet)
plt.xlabel("log( # of nodes )")
plt.ylabel("log( # of edges )")
plt.colorbar(ec[3])

plt.subplot(1,2,2)
ec=plt.hist2d(np.log(x[:,0]), np.log(x[:,2]), bins=(50,50), cmap=plt.cm.jet)
plt.xlabel("log( # of nodes )")
plt.ylabel("log( mean degree )")
plt.colorbar(ec[3])
plt.savefig(os.path.join(image_directory, "subgraph_analysis.png"))


# plot distribution of demand volumes
demands = network.load_full_demands()
print("number of demands : ", len(demands))
demands = np.array(demands)
plt.figure(figsize=(15,5))
plt.title("Demand volume distribution")
plt.hist(demands[:,2], bins=100)
plt.xlabel("Demand volume")
plt.ylabel("# of demands")
plt.savefig(os.path.join(image_directory, "demand_analysis.png"))

# plot distribution of number of chains and mean chain length
plt.figure(figsize=(15,5))
plt.subplot(1,2,1)
plt.title("Chain count distribution")
plt.hist(chain_count, bins=100)
plt.xlabel("# chains")
plt.ylabel("# subgraphs")

plt.subplot(1,2,2)
plt.title("Chain mean length distribution")
plt.hist(chain_length, bins=100)
plt.xlabel("chain length")
plt.ylabel("# subgraphs")
plt.savefig(os.path.join(image_directory, "subgraph_chain_analysis.png"))

plt.savefig("demand_analysis.png")


