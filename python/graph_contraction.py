import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
from networkx.algorithms.components.weakly_connected import number_weakly_connected_components, weakly_connected_components

import network

def component_node_features(component, node, G, forward=True):
    """Compute the features given to the arc (component_node, node) if forward, (node, component) if not forward
    """
    features = []
    for c in component:
        if G.has_edge(node,c) and not forward:
            features.append([G.edges[node,c]["cost"], G.edges[node,c]["bandwidth"]])
        if G.has_edge(c,node) and forward:
            features.append([G.edges[c,node]["cost"], G.edges[c,node]["bandwidth"]])
    if len(features)==1:
        return features[0]
    return np.min(features, axis=0)
 
def contract_graph(G, exclude_nodes=[]):
    """Constructs a new graph H that is defined as G in which all chains have been replaced by 
    a new node.
    """
    # new node increment 
    new_node_inc = max(G.nodes) + 1
    # find all nodes that have degree greater then 2 (4 if graph is directed)
    removable_nodes = [n for n in G.nodes if G.degree(n)>4] + exclude_nodes
    # subgraph with those nodes removed
    subG = G.subgraph([n for n in G.nodes if not n in removable_nodes])
    # mapping of the original nodes to new nodes
    node_map = {n:n for n in removable_nodes}
    components = list(weakly_connected_components(subG))
    for component in components:
        if len(component)==1:
            c = component.pop()
            removable_nodes += [c]
            node_map[c] = c

    cG = nx.DiGraph(G.subgraph(removable_nodes))
    for component in components:
        component = list(component)
        if len(component)>1:
            # add new node to the graph
            new_node = new_node_inc
            new_node_inc += 1
            #removable_nodes.append(new_node)
            for n in component:
                node_map[n] = new_node

            for u in component:
                for v in G.predecessors(u):
                    if node_map[u] != node_map[v]:
                        # cost and bandwidth values of arc node_map[v],node_map[u] = min cost v,u
                        costs = component_node_features(component, v, G, forward=False)
                        cG.add_edge(node_map[v], node_map[u], 
                            cost=int(costs[0]), 
                            bandwidth=int(costs[1]))
                for v in G.successors(u):
                    if node_map[u] != node_map[v]:
                        costs = component_node_features(component, v, G, forward=True)
                        cG.add_edge(node_map[u], node_map[v], 
                            cost=int(costs[0]), 
                            bandwidth=int(costs[1]))

 
    return cG, node_map


if __name__=="__main__":
    plt.figure()
    plt.subplot(2,1,1)
    plt.title("Original graph")
    G = network.get_subgraph(1, 12)
    pos = nx.circular_layout(G)
    nx.draw_networkx(G, pos=pos)
    nx.draw_networkx_edge_labels(G, 
        pos=pos, 
        edge_labels={(u,v):f"{ed['cost']}\n{ed['bandwidth']}" for u,v,ed in G.edges(data=True)},
        label_pos = .2,
    )

    plt.subplot(2,1,2)
    plt.title("Chain-contracted graph")
    cG,_ = contract_graph(G)
    pos = nx.circular_layout(cG)
    nx.draw_networkx(cG, pos=pos)
    nx.draw_networkx_edge_labels(cG, 
        pos=pos, 
        edge_labels={(u,v):f"{ed['cost']}\n{ed['bandwidth']}" for u,v,ed in cG.edges(data=True)},
        label_pos = .2,
    )

    plt.show()
