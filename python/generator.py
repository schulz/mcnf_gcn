import time
import os
import network
import networkx as nx
import graph_to_mps
import numpy as np
from progress.bar import Bar
import matplotlib.pyplot as plt

import torch
from torch_geometric.data import (InMemoryDataset, Data)
from torch_geometric.data import DataLoader
from torchmetrics import F1Score, Precision, Recall, Accuracy

from dataset import GraphDataset, MyData, MyTransform, create_data_object

# number of instance we want to solve
N = 1000
# subgraph level and id
sg_lvl, sg_id = 1,1
# number of demands
n_k = 4

output_dir = f"subgraph_{sg_lvl}_{sg_id}_dataset_N{N}_k{n_k}"
os.makedirs(output_dir, exist_ok=True)
#if os.path.exists(output_dir):
#    os.system(f"rm -f {output_dir}/*")

# choose a graph
G = network.get_subgraph(1, 1)

print(G)

def get_demands(G, n):
    original_demands = np.array(network.load_full_demands())
    demands = []
    # only choose source and destination in layer 0 or 4
    nodes = [n for n in G.nodes if G.nodes[n]["layer"]==0 or G.nodes[n]["layer"]==4]
    for i in range(n):
        [source,dest] = np.random.choice(nodes, replace=False, size=2)
        bandwidth = np.random.choice(original_demands[:,2])
        demands.append([source,dest,bandwidth])
    return np.array(demands)


name_train = output_dir #"subgraph_1_1_dataset_N100_k10"
train_data_path = name_train
processed_data_root = "."
bias_threshold = 0.
batch_size = 32
epochs = 1000
hidden = 64
learning_rate=1e-4
train_dataset = GraphDataset(name_train, processed_data_root, train_data_path, bias_threshold,
                                 transform=MyTransform()).shuffle()

from edgeconv_gcn import EdgeConvSimpleNet
from sklearn.model_selection import train_test_split
# GCN with a hidden dimension of 10, mean aggregation, and 1 layer
model = EdgeConvSimpleNet(n_k, hidden, "mean", 4, node_dim=n_k)

model_name = model.__class__.__name__
if len(model_name)==0:
    model_name = f"{model_cls._class_prefix}_{name_train}"

print(f"Model name: {model_name}")
# metrics 
metrics = [Accuracy(num_classes=2),
           F1Score(num_classes=2, average="macro"),
           Precision(num_classes=2, average="macro"),
           Recall(num_classes=2, average="macro"),
        ]

# training settings
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
#scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min',
#                                                       factor=0.8, patience=args.patience,
#                                                       min_lr=args.min_learning_rate)


train_index, val_index = train_test_split(list(range(0, len(train_dataset))), test_size=.1)
#train_index, val_index = train_test_split(train_index, test_size=.2)

# testing model
o = model(train_dataset[0])
print("model output : ", o)
print("model output shape: ", o.shape)

val_dataset = train_dataset[val_index].shuffle()
#test_dataset = train_dataset[test_index].shuffle() #test_dataset.shuffle()
train_dataset = train_dataset[train_index].shuffle()

train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
val_loader = DataLoader(val_dataset, batch_size=batch_size, shuffle=True)
#test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=True)

# train the model
torch.cuda.empty_cache()

import train
training_start_t = time.time()
history = train.train_model(model, epochs, train_loader, val_loader, optimizer, model_name=model_name, metrics=metrics, batch_size=batch_size)
training_end_t = time.time()
t_train = training_end_t - training_start_t


exit()


with Bar(f"Solving with {n_k} demands: ", max = N) as bar:
    i=0
    while i<N:
        demands = get_demands(G, n_k)
        # vector of demand bandwidths
        demand_bandwidths = np.array([d[2] for d in demands])
        cpx = graph_to_mps.get_mcnf_solver(G, demands, cost="cost", capacity="bandwidth")
        cpx.solve()
        status = cpx.solution.get_status_string()
        if status=="infeasible":
            pass
        else:
            cpx.write(os.path.join(output_dir, f"{i}.mps"))
            cpx.solution.write(os.path.join(output_dir, f"{i}.sol"))
            # make the networkx graph representing the problem
            demand_features = {n: np.zeros(n_k) for n in G.nodes}
            nx.set_node_attributes(G, demand_features, "demands")
            for j,[s,t,dk] in enumerate(demands):
                G.nodes[s]["demands"][j] = dk
                G.nodes[t]["demands"][j] = -dk
            solution = cpx.solution.get_values()
            #print("n_edges ", len(G.edges), ", sol len: ", len(solution))
            edge_labels = {}
            for j,e in enumerate(G.edges):
                sol = np.array(solution[j*n_k:(j+1)*n_k])
                #print("edge ", j, e, sol)
                edge_labels[e] = np.array(solution[j*n_k:(j+1)*n_k])
                s = np.sum(edge_labels[e])
                if s!=0.:
                    edge_labels[e] /= s
                ## normalize the labels with regard to the bandwith of the demands
                #edge_labels[e][demand_bandwidths!=0.] = sol[demand_bandwidths!=0.]/ demand_bandwidths[demand_bandwidths!=0.]
            nx.set_edge_attributes(G, edge_labels, "label")
            node_labels={}
            for n in G.nodes:
                node_labels[n] = np.zeros(n_k)
                #for j in G.neighbors(n):
                #    node_labels[n] += edge_labels[n,j]
                for j in G.predecessors(n):
                    node_labels[n] += edge_labels[j,n]
                for k in range(n_k):
                    if n==demands[k][0] or n==demands[k][1]:
                        node_labels[n][k] = 1.
            nx.set_node_attributes(G, node_labels, "label")
            tmpG = nx.DiGraph(G)
            graph_path = os.path.join(output_dir, f"{i}_graph.pkl")
            nx.write_gpickle(tmpG, graph_path)
                  

            ## make the data object 
            #data,node2var = create_data_object(G)
            #print(data.demands)
            ## test the model
            #from edgeconv_gcn import EdgeConvSimpleNet
            ## GCN with a hidden dimension of 10, mean aggregation, and 1 layer
            #model = EdgeConvSimpleNet(n_k, 10, "mean", 1, node_dim=n_k)
            #model.eval()
            #out = model(data).exp().cpu().detach().numpy()
            #print("Out shape: ", out.shape)
            #print("y shape: ", data.y.shape)
            #print("y_real shape: ", data.y_real.shape)
           

            i += 1
            bar.next()

exit()


number_demands = list(range(1,50+1))
feasible_instances = []
solve_times = []
objective_values = []
for n_k in number_demands:
    print("Number of demands : ", n_k)
    c = 0

    solve_time=[]
    objective_value = []
    with Bar(f"Solving with {n_k} demands: ", max = 100) as bar:
        for i in range(N):
            demands = get_demands(G, n_k)
            cpx = graph_to_mps.get_mcnf_solver(G, demands, cost="cost", capacity="bandwidth")
            t0 = cpx.get_time()
            cpx.solve()
            t0 = cpx.get_time() - t0
            solve_time.append(t0)
            status = cpx.solution.get_status_string()
            if status=="infeasible":
                pass
            else:
                c += 1
                objective_value.append(cpx.solution.get_objective_value())
            bar.next()
    feasible_instances.append(float(c))
    solve_times.append(solve_time)
    objective_values.append(objective_value)

plt.figure()
plt.subplot(3,1,1)
plt.title("Feasible solutions on 100 instances")
plt.plot(number_demands, feasible_instances)
plt.subplot(3,1,2)
plt.title("Runtime distribution")
plt.boxplot(solve_times, labels=number_demands)
plt.subplot(3,1,3)
plt.title("Objective value distribution")
plt.boxplot(objective_values, labels=number_demands)

plt.show()
