import sys
import argparse
import json
import time
from  datetime import datetime
date_fmt = "%d/%m/%YT%H:%M:%S.%f"

from progress.bar import Bar

import matplotlib.pyplot as plt

import os
import os.path as osp

from sklearn.model_selection import train_test_split

from torchmetrics import F1Score, Precision, Recall, Accuracy

#from torch_geometric.data import (InMemoryDataset, Data)
from torch_geometric.loader import DataLoader

import numpy as np
#import pandas as pd
import torch
import torch.nn.functional as F

#from mipgnn.gnn_models import manager as gnn_manager

#from mipgnn.predict import create_data_object

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

bias_threshold = 0.

from dataset import GraphDataset, MyData, MyTransform, create_data_object

def is_int(s):
    """Check is variable is int
    """
    try:
        i = int(s)
        return True
    except:
        return False

def train_epoch_model(model, dataloader, optimizer, device=None, batch_size=10):
    """Trains model for one epoch
    """
    #if device is None:
    #    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    #model.to(device)
    model.train()

    # loss_all = 0
    zero = torch.tensor([0]).to(device)
    one = torch.tensor([1]).to(device)

    loss_all = 0
    c = 0
    with Bar("epoch ", max=len(dataloader)) as bar:
        for data in dataloader:
            data = data.to(device)
            c += batch_size
    
            y = data.y_real
    
            optimizer.zero_grad()
            output = model(data)
    
            #loss = F.nll_loss(output, y)
            #loss = F.cross_entropy(output, y.float()) 
            mask = (y > -10.)
            if output.shape != y.shape:
                raise Exception("Bad shapes ", output.shape, y.shape)
            ns = (1, data.var_edges.shape[0],data.num_demands.item())
            # binary classifier for edge usage
            y_edge_usage = y.reshape(ns)
            y_edge_usage = (torch.sum(y_edge_usage, axis=-1) > 0.).to(torch.float)
            
            pred_edge_usage = (torch.sum(output.reshape(ns), axis=-1) > 0.).to(torch.float)
            loss = F.binary_cross_entropy(pred_edge_usage, y_edge_usage)
            # binary classifier for edge-demand usage
            y_edge_demand_usage = (y > 0.).to(torch.float)
            pred_edge_demand_usage = (output > 0.).to(torch.float)
            loss += F.binary_cross_entropy(torch.masked_select(y_edge_demand_usage, mask), torch.masked_select(pred_edge_demand_usage, mask))
            # regression
            loss = F.kl_div(torch.masked_select(output,mask), torch.masked_select(y.float(),mask))
    
            loss.backward()
            loss_all += batch_size * loss.item()
            optimizer.step()
            bar.next()
    l = loss_all / c
    return loss_all / c

def train_model(model, epochs, train_loader, val_loader, optimizer, device=None, batch_size=10, metrics=[], model_name="generic_model", output_dir=None):
    """Train model for a given number of epochs. Returns the training history.
    """
    # training history
    history = []
    
    # target directory
    model_directory = os.path.expanduser("~/.cache/mipgnn/models")
    model_log_path = os.path.join(model_directory, model_name+".log")
    model_path = os.path.join(model_directory, model_name)
    os.makedirs(model_directory, exist_ok=True)
    
    losses = [] 
    mtrcs = []
    with Bar(f"Training {model_name}: ", max=epochs) as bar:
        for epoch in range(epochs):
            train_loss = train_epoch_model(model, train_loader, optimizer, batch_size=batch_size)
    
            [val_loss, acc, f1, prec, recall ] = evaluate_model(model, val_loader, metrics=metrics)
            losses.append([train_loss, val_loss])
            mtrcs.append([acc,f1,prec,recall])
            loss_image_path = os.path.join(output_dir, "train_val_loss.png")
            metrics_image_path = os.path.join(output_dir, "val_metrics.png")
            
            checkpoint_path = os.path.join(output_dir, "checkpoint.pt")
            torch.save({
                        'epoch': epoch,
                        'model_state_dict': model.state_dict(),
                        'optimizer_state_dict': optimizer.state_dict(),
                        'loss': train_loss,
                        }, checkpoint_path)

            plt.figure(figsize=(5,10))
            plt.subplot(2,1,1)
            plt.title("Train loss")
            plt.plot([h[0] for h in losses])
            #plt.yscale("log")
            plt.subplot(2,1,2)
            plt.title("Validation loss")
            plt.plot([h[1] for h in losses])
            #plt.yscale("log")
            plt.savefig(loss_image_path)

            plt.figure(figsize=(5,10))
            for i in range(len(metrics)):
                plt.subplot(len(metrics),1,i+1)
                plt.title("Validation "+metrics[i].__class__.__name__)
                plt.plot([m[i] for m in mtrcs])
            plt.savefig(metrics_image_path)
 
            plt.close("all")


            bar.next()
    history = np.array(losses)

    return history



@torch.no_grad()
def evaluate_model(model, dataloader, device=None, metrics=[]):
    """Evaluate the model
    """
    if device is None:
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    model.eval()

    zero = torch.tensor([0]).to(device)
    one = torch.tensor([1]).to(device)

    metrics = [metric.to(device) for metric in metrics]

    first = True
    loss_all  = 0.
    c = 0
    meters=[]
    for data in dataloader:
        data = data.to(device)
        c += len(data)
        pred = model(data)

        # reconstruct the solution and check constraint violations
        
        
        y = data.y_real
        ns = (1, data.var_edges.shape[0],data.num_demands.item())

        mask = (y>-10.)
        

        y_edge_usage = y.reshape(ns)
        y_edge_usage = (torch.sum(y_edge_usage, axis=-1) > 0.).to(torch.float)
        
        pred_edge_usage = (torch.sum(pred.reshape(ns), axis=-1) > 0.).to(torch.float)
        loss = F.binary_cross_entropy(pred_edge_usage, y_edge_usage)
        # binary classifier for edge-demand usage
        y_edge_demand_usage = (y > 0.).to(torch.float)
        pred_edge_demand_usage = (pred> 0.).to(torch.float)
        loss += F.binary_cross_entropy(torch.masked_select(y_edge_demand_usage, mask), torch.masked_select(pred_edge_demand_usage, mask))
        # regression
        #loss = F.kl_div(torch.masked_select(pred,mask), torch.masked_select(y.float(),mask))

        loss = F.kl_div(torch.masked_select(pred,mask), torch.masked_select(y.float(),mask))

        loss_all += len(data) * loss.item()

        pred_used = torch.flatten(torch.where(pred.exp() > 0.5, one, zero))
        y_bin = torch.flatten(torch.where(y > 0., one, zero))
        meters.append([m(pred_used, y_bin) for m in metrics])


    return [loss_all/c] + np.mean(meters, axis=0).tolist()#+[m(pred_all, y_all) for m in metrics]



if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("model", type=str, help="model that you want to train")
    parser.add_argument("--model-args", type=str, help="model constructor arguments as JSON", default=gnn_manager.DEFAULT_MODEL_ARGS_STR)
    parser.add_argument("--name", type=str, help="name of the trained model", default="")
    parser.add_argument("--train-data", type=str, help="path to training data")
    parser.add_argument("--validation-split", type=float, help="validation split (default=0.)", default=0.)
    parser.add_argument("--bias-threshold", type=float, help="bias threshold (default=0.)", default=0.)
    parser.add_argument("--batch-size", type=int, help="batch size (default=10)", default=10)
    parser.add_argument("--epochs", type=int, help="number of epochs (default=30)", default=30)
    parser.add_argument("--learning-rate", type=float, help="learning rate (default=0.001)", default=0.001)
    parser.add_argument("--min-learning-rate", type=float, help="minimum learning rate (default=0.0000001)", default=0.0000001)
    parser.add_argument("--patience", type=int, help="scheduler patience (default=10)", default=10)
    parser.add_argument("--train-test-split", type=float, help="split into training and testing sets (default=0.2)", default=0.2)
    parser.add_argument("--list-models", action="store_true", help="show a list of model classes")


    args = parser.parse_args()

    # starting datetime
    start_dt = datetime.now()

    # metrics 
    metrics = [Accuracy(num_classes=2),
               F1Score(num_classes=2, average="macro"),
               Precision(num_classes=2, average="macro"),
               Recall(num_classes=2, average="macro"),
            ]

    # Prepare data.
    bias_threshold = args.bias_threshold
    batch_size = 1#args.batch_size
    num_epochs = args.epochs

    # processed dataset directory
    processed_data_root = os.path.expanduser("~/.cache/mipgnn/datasets")

    # training data
    train_data_path = args.train_data
    if not os.path.exists(train_data_path):
        print(f"Train data path not found : {train_data_path}")

    name_train = os.path.basename(train_data_path)
    print(f"Loading training data {name_train} from {train_data_path}")
    train_dataset = GraphDataset(name_train, processed_data_root, train_data_path, bias_threshold,
                                 transform=MyTransform()).shuffle()
 

    # model
    model_args = json.loads(args.model_args)
    print(f"Training model class: {args.model}")
    print(f"Model arguments:")
    print(json.dumps(model_args, indent=4, sort_keys=True))
    
    if is_int(args.model):
        model_cls = gnn_manager.get_model_class_instance(index=int(args.model))
    else:
        model_cls = gnn_manager.get_model_class_instance(name=args.model)
    model = model_cls(**model_args).to(device)
    model_name = args.name
    if len(model_name)==0:
        model_name = f"{model_cls._class_prefix}_{name_train}"

    print(f"Model name: {model_name}")
    
    # training settings
    optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min',
                                                           factor=0.8, patience=args.patience,
                                                           min_lr=args.min_learning_rate)
 

    train_index, val_index = train_test_split(list(range(0, len(train_dataset))), test_size=args.train_test_split)
    #train_index, val_index = train_test_split(train_index, test_size=.2)

    val_dataset = train_dataset[val_index].shuffle()
    #test_dataset = train_dataset[test_index].shuffle() #test_dataset.shuffle()
    train_dataset = train_dataset[train_index].shuffle()

    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    val_loader = DataLoader(val_dataset, batch_size=batch_size, shuffle=True)
    #test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=True)

    # train the model
    torch.cuda.empty_cache()
    
    training_start_t = time.time()
    history = train_model(model, args.epochs, train_loader, val_loader, optimizer, model_name=model_name, metrics=metrics)
    training_end_t = time.time()
    t_train = training_end_t - training_start_t

    # save the models training arguments
    model_data = {"name": model_name,
            "datetime": datetime.strftime(start_dt, date_fmt),
            "model_class":model_cls.__name__,
            "model_args": json.loads(args.model_args),
            "epochs": args.epochs,
            "learining_rate": args.learning_rate,
            "patience": args.patience,
            "min_learning_rate": args.min_learning_rate,
            "train_dataset": name_train,
            "train_test_split": args.train_test_split,
            "times":{
                "training_start": training_start_t,
                "training_end": training_end_t,
                "training_seconds": t_train,
            }
            }
    model_data_path= os.path.expanduser(f"~/.cache/mipgnn/models/{model_name}.json")
    with open(model_data_path, "w") as f:
        f.write(json.dumps(model_data, indent=4, sort_keys=True))

    torch.cuda.empty_cache()
   


