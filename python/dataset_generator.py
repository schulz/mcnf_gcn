"""
Generating datasets of MCNF problems.

Each dataset is a collection of N MCNF instances : 
    - {0..N}.mps : the LP file containing the MCNF instance, loadable with CPLEX
    - {0..N}.sol : solution file
    - {0..N}_graph.pkl : graph file

Type 1 : constant graph G=(V,E), costs, capacities and number of demands |K|. Demands created by taking two nodes at random and a bandwidth taken at random from the full list of demands. Nodes have no features, edge features are [cost_ij, capacity_ij]. 

Type 2 : constant graph G=(V,E), costs, capacities and number of demands |K|. Demands created as in Type 1. A node demand_k is added to the graph for every demand as well as arcs (demand_k, o_k), (t_k, demand_k). Each demand node is given a feature equals to the demands bandwidth. 


"""
import time
import copy
import os
import network
import networkx as nx
import graph_to_mps
import numpy as np
from progress.bar import Bar
import matplotlib.pyplot as plt

import torch
from torch_geometric.data import (InMemoryDataset, Data)
from torch_geometric.data import DataLoader
from torchmetrics import F1Score, Precision, Recall, Accuracy

from dataset import GraphDataset, MyData, MyTransform, create_data_object


def get_demands(G, n):
    original_demands = np.array(network.load_full_demands())
    demands = []
    # only choose source and destination in layer 0 or 4
    nodes = [n for n in G.nodes if G.nodes[n]["layer"]==0 or G.nodes[n]["layer"]==4]
    for i in range(n):
        [source,dest] = np.random.choice(nodes, replace=False, size=2)
        bandwidth = np.random.choice(original_demands[:,2])
        demands.append([source,dest,bandwidth])
    r=np.array(demands)
    return r

def make_instance(output_dir, dataset_type=1):
    pass

def make_dataset(n, graph, n_demands, output_dir=None, dataset_type=2):
    if output_dir is None:
        output_dir = f"dataset_1_2_n{n}_k{n_demands}_type{dataset_type}"
    if os.path.exists(output_dir):
        os.system(f"rm -rf {output_dir}/*")
    os.makedirs(output_dir, exist_ok=True)

    with Bar(f"Solving with {n_demands} demands: ", max = n) as bar:
        i=0
        while i<n:
            # copy the graph
            G = nx.DiGraph(graph)

            # clear all demand info
            nx.set_node_attributes(G, {n:[] for n in G.nodes}, "node_demands")
            demands = get_demands(G, n_demands)
  
            # vector of demand bandwidths
            demand_bandwidths = np.array([d[2] for d in demands])
            cpx = graph_to_mps.get_mcnf_solver(G, demands, cost="cost", capacity="bandwidth")
            cpx.solve()
            status = cpx.solution.get_status_string()
            if status=="infeasible":
                print("infeasible", flush=True)
                pass
            else:
                # write the .mps file
                cpx.write(os.path.join(output_dir, f"{i}.mps"))
                # write the .sol file
                cpx.solution.write(os.path.join(output_dir, f"{i}.sol"))
                # make the networkx graph representing the problem
                if dataset_type == 2:
                    # add a node for every demand
                    for k,demand in enumerate(demands):
                        G.add_node(f"d{k}", type= "demand")
                        G.add_edge(f"d{k}", demand[0], cost= 0., bandwidth= 0.)
                        G.add_edge(demand[1], f"d{k}", cost= 0., bandwidth= 0.)
                        
                demand_features = {n: np.zeros(1) for n in G.nodes}
                for k,demand in enumerate(demands):
                    demand_features[f"d{k}"] += demand[2]
                nx.set_node_attributes(G, demand_features, "demands")
                #for j,[s,t,dk] in enumerate(demands):
                #    graph.nodes[s]["demands"][j] = dk
                #    graph.nodes[t]["demands"][j] = -dk
                solution = cpx.solution.get_values()
                edge_labels = {}
                var_edges = [e for e in G.edges if G.nodes[e[0]].get("type","")!="demand" and G.nodes[e[1]].get("type","")!="demand"]
                for j,e in enumerate(var_edges):
                    if G.nodes[e[0]].get("type","")=="demand" or G.nodes[e[1]].get("type","")=="demand":
                        continue
                    sol = np.array(solution[j*n_demands:(j+1)*n_demands])
                    edge_labels[e] = np.array(solution[j*n_demands:(j+1)*n_demands])
                    s = np.sum(edge_labels[e])
                    if s!=0.:
                        edge_labels[e] /= s
                nx.set_edge_attributes(G, edge_labels, "label")
                # demand information
                for demand in demands:
                    G.nodes[demand[0]]["node_demands"].append(demand)
                # check number of demands 
                _demands = []
                for node in G.nodes:
                    if G.nodes[node].get("node_demands",[]):
                        _demands += G.nodes[node]["node_demands"]

                tmpG = nx.DiGraph(G)
                graph_path = os.path.join(output_dir, f"{i}_graph.pkl")
                nx.write_gpickle(tmpG, graph_path)
                      
    
                i += 1
                bar.next()

if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("n", help="number of samples to generate", type=int, default=1000)
    parser.add_argument("n_demands", help="number of demands per instance", type=int, default=10)
    parser.add_argument("output_directory", help="output directory", type=str, default="")
    parser.add_argument("--subgraph-level", help="subgraph level (default=1)", type=int, default=1)
    parser.add_argument("--subgraph-id", help="subgraph id (default=1)", type=int, default=1)


    args = parser.parse_args()
    print(args)

    # choose a graph
    G = network.get_subgraph(args.subgraph_level, args.subgraph_id)
    print(G)
    
    if len(args.output_directory)==0:
        args.output_directory = None
    make_dataset(args.n, G, args.n_demands, output_dir=args.output_directory)


