import itertools
import json
import time
import os
import network
import networkx as nx
import numpy as np
import pickle as pkl
from progress.bar import Bar
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split



import torch
from torch_geometric.data import (InMemoryDataset, Data)
from torch_geometric.data import DataLoader
from torchmetrics import F1Score, Precision, Recall, Accuracy

from dataset import GraphDataset, MyData, MyTransform, create_data_object, load_dataset
from edgeconv_gcn import EdgeConvSimpleNet


def write_metadata(data, filename):
    with open(filename, "w") as f:
        f.write(json.dumps(data, indent=4, sort_keys=True))


def do_training(train_dataset_path, bias_threshold, batch_size, epochs, hidden, l_layers, learning_rate, min_learning_rate, patience, output_dir, checkpoint_path):

    # load the training dataset
    train_dataset = load_dataset(train_dataset_path, bias_threshold)
    
    # create the output directory
    os.makedirs(output_dir, exist_ok=True)
    
    # training metadata
    metadata = {"learning_rate": learning_rate,
                "n_layers": hidden,
                "dataset_name": train_dataset_name,
                "test_split": .1,
                "hidden": hidden, 
                "batch_size": batch_size,
                "epochs": epochs,
                "checkpoint": checkpoint_path,
                #"lr_scheduler": {
                #  "patience": patience,
                #  "min_learning_rate": min_learning_rate,
                #  "factor": .8,
                #  "mode": "min"
                #  "class": ReduceLROnPlateau,
                #}
               }
    metadata_path = os.path.join(output_dir, "metadata.json")
    write_metadata(metadata, metadata_path)
    
    # GCN with a hidden dimension of 10, mean aggregation, and 1 layer
    model = EdgeConvSimpleNet(1, hidden, "mean", n_layers, node_dim=3)
    
    model_name = model.__class__.__name__
    if len(model_name)==0:
        model_name = f"{model_cls._class_prefix}_{name_train}"
    
    # metrics 
    metrics = [Accuracy(num_classes=2),
               F1Score(num_classes=2, average="macro"),
               Precision(num_classes=2, average="macro"),
               Recall(num_classes=2, average="macro"),
            ]
    
    # training settings
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min',
                                                           factor=0.8, patience=patience,
                                                           min_lr=min_learning_rate)

    if os.path.exists(checkpoint_path):
        checkpoint = torch.load(checkpoint_path)
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    
    
    # split the training data
    train_index, val_index = train_test_split(list(range(0, len(train_dataset))), test_size=.1)
    
    val_dataset = train_dataset[val_index].shuffle()
    train_dataset = train_dataset[train_index].shuffle()
    
    train_loader = DataLoader(train_dataset, 
                              batch_size=batch_size, 
                              shuffle=True, 
                              follow_batch=["y", "y_real", "node_features", "obj", "edge_features", "demands"])
     
    val_loader = DataLoader(val_dataset, batch_size=batch_size, shuffle=True)
    
    # train the model
    torch.cuda.empty_cache()
    
    import train
    training_start_t = time.time()
    history = train.train_model(model, epochs, train_loader, val_loader, optimizer, model_name=model_name, metrics=metrics, batch_size=batch_size, output_dir=output_dir)
    training_end_t = time.time()
    t_train = training_end_t - training_start_t
    metadata["training_time"] = t_train
    write_metadata(metadata, metadata_path)
    
    pkl.dump(history, open(os.path.join(output_dir, "history.pkl"), "wb"))

if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("train_dataset", help="path to the training dataset", type=str)
    parser.add_argument("-b", "--batch-size", help="batch size (default=1)", type=int, default=1)
    parser.add_argument("-e", "--epochs", help="number of epochs (default=100)", type=int, default=100)
    parser.add_argument("-l", "--layers", help="number of message passing layers (default=4)", type=int, default=4)
    parser.add_argument("-r", "--lr", help="learning rate (default=1e-3)", type=float, default=1e-3)
    parser.add_argument("-p", "--patience", help="training patience (default=10)", type=int, default=10)
    parser.add_argument("-d", "--hidden", help="dimension of the hidden layer (default=64)", type=int, default=64)
    parser.add_argument("-o", "--output-directory", help="output directory (default=.)", type=str, default=".")
    parser.add_argument("--checkpoint-path", help="load model state from checkpoint (default=dont)", type=str, default="")
    
    args = parser.parse_args()
    
    # training dataset path
    train_dataset_path = args.train_dataset
    train_dataset_name = os.path.basename(train_dataset_path)
    
    bias_threshold = 0.
    batch_size = args.batch_size
    
    # training parameters
    epochs = args.epochs
    hidden = args.hidden # number of hidden dimensions
    n_layers = args.layers
    learning_rate= args.lr
    min_learning_rate = 1e-10
    patience = args.patience
    output_dir = args.output_directory

    do_training(train_dataset_path, bias_threshold, batch_size, epochs, hidden, n_layers, learning_rate, min_learning_rate, patience, output_dir, args.checkpoint_path)

   

exit()


 
#num_layers = [2, 4, 8]
#learning_rates = [1e-2,1e-4, 1e-6]
#min_learning_rate = 1e-10
#patience = 10
#epochs = 1000
#output_dir = f"{name_train}_test_outputs_3"
#os.makedirs(output_dir, exist_ok=True)
#
#for i,[n_layers, lr] in enumerate(itertools.product(num_layers, learning_rates)):
#    # directory
#    test_dir = os.path.join(output_dir, str(i))
#    os.makedirs(test_dir, exist_ok=True)
#    
#    # training metadata
#    metadata = {"learning_rate": lr,
#                "n_layers": n_layers,
#                "dataset_name": name_train,
#                "test_split": .1,
#                "hidden": hidden, 
#                "batch_size": batch_size,
#                "epochs": epochs,
#                #"lr_scheduler": {
#                #  "patience": patience,
#                #  "min_learning_rate": min_learning_rate,
#                #  "factor": .8,
#                #  "mode": "min"
#                #  "class": ReduceLROnPlateau,
#                #}
#               }
#    #print(json.dumps(metadata, indent=4, sort_keys=True))
#    metadata_path = os.path.join(test_dir, "metadata.json")
#    write_metadata(metadata, metadata_path)
#    
#    # GCN with a hidden dimension of 10, mean aggregation, and 1 layer
#    model = EdgeConvSimpleNet(1, hidden, "mean", n_layers, node_dim=3)
#    
#    model_name = model.__class__.__name__
#    if len(model_name)==0:
#        model_name = f"{model_cls._class_prefix}_{name_train}"
#    
#    # metrics 
#    metrics = [Accuracy(num_classes=2),
#               F1Score(num_classes=2, average="macro"),
#               Precision(num_classes=2, average="macro"),
#               Recall(num_classes=2, average="macro"),
#            ]
#    
#    # training settings
#    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
#    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min',
#                                                           factor=0.8, patience=patience,
#                                                           min_lr=min_learning_rate)
#    
#    
#    train_index, val_index = train_test_split(list(range(0, len(train_dataset))), test_size=.1)
#    
#    # testing model
#    val_dataset = train_dataset[val_index].shuffle()
#    train_dataset = train_dataset[train_index].shuffle()
#    
#    train_loader = DataLoader(train_dataset, 
#                              batch_size=batch_size, 
#                              shuffle=True, 
#                              follow_batch=["y", "y_real", "node_features", "obj", "edge_features", "demands"])
#     
#    val_loader = DataLoader(val_dataset, batch_size=batch_size, shuffle=True)
#    
#    # train the model
#    torch.cuda.empty_cache()
#    
#    import train
#    training_start_t = time.time()
#    history = train.train_model(model, epochs, train_loader, val_loader, optimizer, model_name=model_name, metrics=metrics, batch_size=batch_size, output_dir=test_dir)
#    training_end_t = time.time()
#    t_train = training_end_t - training_start_t
#    metadata["training_time"] = t_train
#    write_metadata(metadata, metadata_path)
#    
#    pkl.dump(history, open(os.path.join(test_dir, "history.pkl"), "wb"))


