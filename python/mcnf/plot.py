import networkx as nx
import matplotlib.pyplot as plt
import numpy as np

def plot_mcnf_instance(graph, demands, node_color="#1f78b4", ax=None, cost_name="cost", capacity_name="bandwidth"):
    # plot the graph and demands
    pos = nx.circular_layout(graph)
    if ax is None:
        fig, ax=plt.subplots(1,1)
    nx.draw_networkx(graph, pos=pos, with_labels=True, node_color=node_color, ax=ax)
    demand_G = nx.DiGraph()
    for k in demands:
        demand_G.add_node(k[0])
        demand_G.add_node(k[1])
        demand_G.add_edge(k[0], k[1])

    nx.draw_networkx(demand_G, pos=pos, edge_color="g", width=4, style="dashed", alpha=.5, arrowsize=16, ax=ax)
    if not graph.is_multigraph():
        nx.draw_networkx_edge_labels(
            graph, pos,
            edge_labels={(u,v):f"{graph.edges[u,v][cost_name]}\n{graph.edges[u,v][capacity_name]}" for u,v in graph.edges},
            font_color='red',
            rotate=False,
            label_pos=0.2,
            ax=ax,
            
        )
        nx.draw_networkx_edge_labels(
            graph, pos,
            edge_labels={(k[0],k[1]):k[2] for k in demands},
            font_color='blue',
            rotate=False,
            label_pos=0.2,
            ax=ax,
            
        )

    return ax

def plot_mcnf_solution(graph, demands, solution):
    sol = np.array(solution)
    n_k = len(demands)
    n_edges = int(len(solution) / n_k)
    pos = nx.circular_layout(graph)
    f, ax = plt.subplots(1,n_k)
    if n_k==1:
        ax = [ax]
    for k,demand in enumerate(demands):
        # plot the graph and demands
        nx.draw_networkx(graph, pos=pos, with_labels=True, ax=ax[k])
        demand_G = nx.DiGraph()
        demand_G.add_node(demand[0])
        demand_G.add_node(demand[1])
        demand_G.add_edge(demand[0], demand[1])

        nx.draw_networkx(demand_G, pos=pos, edge_color="g", width=4, style="dashed", alpha=.5, arrowsize=16, ax=ax[k])
        nx.draw_networkx_edge_labels(
            graph, pos,
            edge_labels={(demand[0],demand[1]):demand[2]},
            font_color='blue',
            rotate=False,
            label_pos=0.2,
            ax=ax[k]
        )
        # get flow on each edge attributed to current demand
        edge_k_flows = {}
        for i,edge in enumerate(graph.edges):
            # all flows through current edge
            tot_flow = np.sum(sol[i*n_k:(i+1)*n_k])
            # flow attributable to demand k
            k_flow = sol[i*n_k+k]
            if k_flow:
                edge_k_flows[edge] = f"{int(k_flow)}/{int(tot_flow)}\n{round(100*k_flow / tot_flow,2)}%"
        nx.draw_networkx_edges(graph, pos, 
            edgelist=[e for e in edge_k_flows if edge_k_flows[e]],
            edge_color="blue",
            alpha=.5,
            width=4,
            arrowsize=10,
            ax=ax[k],
        )
        nx.draw_networkx_edge_labels(
            graph, pos,
            edge_labels=edge_k_flows,
            font_color='green',
            rotate=False,
            #label_pos=0.2,
            ax=ax[k],
        )
        plt.title(f"Demand {k}: {demand[0]}->{demand[1]}, d_{k}={demand[2]}")
        #plt.show()
        #plt.close("all")
    return f, ax


