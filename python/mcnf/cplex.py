import numpy as np
from cplex import Cplex

def get_mcnf_solver(G, demands, cost="cost", capacity="capacity"):
    nodes_map = {n:i for i,n in enumerate(G.nodes)}

    # number of edges in the graph
    n_edges = len(G.edges)
    n_nodes = len(G.nodes)
    n_k = len(demands)
    # number of variables in the problem
    n_var = n_k * n_edges
    # objective function coefficients and demand-arc-index map
    demand_arc_index = {}
    c = []
    for arc in G.edges:
        for k in range(n_k):
            demand_arc_index[k,arc[0], arc[1]] = len(c)
            c.append(G.edges[arc]["cost"])
    c = np.array(c)
    
    # CPLEX solver instance
    cpx = Cplex()
    cpx.parameters.timelimit.set(60)
    cpx.set_log_stream(None)
    cpx.set_warning_stream(None)
    cpx.set_results_stream(None)



    #cpx.set_problem_name("G-MCNF")
    cpx.set_problem_type(cpx.problem_type.LP)
    
    # add variable names
    cpx.variables.add(obj=c.astype(float), lb=np.zeros(n_var), names=[f"x_{i}" for i in range(n_var)])
    
    # add objective
    cpx.objective.set_name("cost")
    cpx.objective.set_sense(cpx.objective.sense.minimize)
    
    ## add upper bound constraints
    constraint_names = [f"c_eq_{i}" for i in range(n_nodes*n_k)]
    constraint_senses = ["E" for i in range(n_nodes*n_k)]
    constraint_rhs=[]
    constraint_lin_expr = []
    for k in range(n_k):
        for i in G.nodes:
            ind,val=[],[]
            b = 0.
            # incoming flow, set to 1
            for j in G.neighbors(i):
                if G.has_edge(i,j):
                    ind.append(demand_arc_index[k,i,j])
                    val.append(1)
            for j in G.predecessors(i):
                t=0
                if G.has_edge(j,i):
                    ind.append(demand_arc_index[k,j,i])
                    val.append(-1)
            if i==demands[k,0]:
                b = demands[k,2]
            if i==demands[k,1]:
                b = -demands[k,2]
            constraint_lin_expr.append(cplex.SparsePair(ind=ind,val=val))
            constraint_rhs.append(b)

    constraint_names += [f"c_ub_{i}" for i in range(n_edges)]
    constraint_senses += ["L" for i in range(n_edges)]
    for i,j in G.edges:
        ind,val=[],[]
        for k in range(n_k):
            ind.append(demand_arc_index[k,i,j])
            val.append(1.)
        constraint_lin_expr.append(cplex.SparsePair(ind=ind,val=val))
        constraint_rhs.append(float(G.edges[i,j][capacity]))
    
    cpx.linear_constraints.add(lin_expr=constraint_lin_expr, 
                               senses=constraint_senses, 
                               rhs=np.array(constraint_rhs), 
                               names=constraint_names)
    return cpx


