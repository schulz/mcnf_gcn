import numpy as np
import matplotlib.pyplot as plt
import network
import networkx as nx

g = network.load_full_graph()

degrees = sorted((d for n,d in g.degree()), reverse=True)

print("mean degree : ", np.mean(degrees))


plt.figure(figsize=(15, 5))
plt.subplot(1,2,1)
plt.plot(degrees, "b-", marker="o")
plt.title("Degree rank plot")
plt.xlabel("Rank")
plt.ylabel("Degree")
plt.subplot(1,2,2)
plt.hist(degrees)
plt.title("Degree histogram")
plt.xlabel("Degree")
plt.ylabel("# of nodes")
plt.savefig("full_graph_degree_analysis.png")
