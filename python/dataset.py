import sys
import os
import torch
import networkx as nx
import numpy as np
import pickle as pkl

from progress.bar import Bar

from torch_geometric.data import (InMemoryDataset, Data)
from torch_geometric.loader import DataLoader

#from generator import create_data_object



def create_data_object(graph, bias_threshold=0.):
    """Takes a graph object and creates a data structure that can be pickled and loaded with torch_geometric
    """
    node_to_var = {}
    var_nodes = []
    for i, (node, node_data) in enumerate(graph.nodes(data=True)):
        # Node is a variable node.
        #if node_data['bipartite'] == 0 or node_data["type"]=="variable":
        node_to_var[node] = i
        if node_data.get("type","")!="demand":
            var_nodes.append(i)



    # Make graph directed.
    graph = nx.convert_node_labels_to_integers(graph)
    graph = graph.to_directed() if not nx.is_directed(graph) else graph
    data = Data()

    #  Maps networkx ids to new variable node ids.
    #  Maps networkx ids to new constraint node ids.
    edge_to_var = {}

    # Number of variables.
    num_nodes = len(graph.nodes)
    # Number of constraints.
    num_edges = len(graph.edges)
    # Targets (classes).
    y = []
    y_real = []
    # Features for variable nodes.
    feat_var = []
    # Right-hand sides of equations.
    feat_rhs = []

    index = []
    index_var = []
    obj = []

    node_features = []


    demands = []
    def has_demand(d, d_lst):
        _d=np.array([node_to_var[d[0]], node_to_var[d[1]], d[2]])
        for i in range(len(d_lst)):
            if np.all(np.array(d_lst[i]) == _d):
                return True
        return False

    # Iterate over nodes, and collect features.
    for i, (node, node_data) in enumerate(graph.nodes(data=True)):
        # Node is a variable node.
        #if node_data['bipartite'] == 0 or node_data["type"]=="variable":
        #node_to_var[node] = i
        node_features.append(node_data["demands"])
        index_var.append(0)
        #y.append(np.array(node_data["label"]) > bias_threshold)
        #y_real.append(node_data["label"])
        
        node_demands = node_data.get("node_demands", None)
        if node_demands:
            for nd in node_demands:
                if not has_demand(nd, demands) or True:
                    demands += [np.array([node_to_var[nd[0]], node_to_var[nd[1]], nd[2]])]
    demands = np.array(demands, dtype=int)


    for i,(s, t, edge_data) in enumerate(graph.edges(data=True)):
        if graph.nodes[s].get("type","")=="demand" or graph.nodes[t].get("type","")=="demand":
            continue
        y += (edge_data["label"]>bias_threshold).tolist()
        y_real += [edge_data["label"].tolist()]


    # Edge list for var->con graphs.
    edge_list = []

    # Create features matrices for variable nodes.
    edge_features = []

    var_edges = []

    # Flow of messages: source -> target.
    for i, (s, t, edge_data) in enumerate(graph.edges(data=True)):
        #edge_list.append([node_to_var[s], node_to_var[t]])
        edge_list.append([s, t])

        edge_features.append([edge_data['cost'], edge_data['bandwidth']])
        obj.append(edge_data['cost'])
        #y.append(np.array(edge_data["label"]) > bias_threshold)
        #y_real.append(edge_data["label"])
        if graph.nodes[s].get("type","")!="demand" and graph.nodes[t].get("type","")!="demand":
            var_edges.append(i)


    edge_index = torch.tensor(edge_list).t().contiguous()

    # Create data object.
    data.edge_index = edge_index

    data.y = torch.from_numpy(np.array(y)).to(torch.long)
    data.y_real = torch.from_numpy(np.array(y_real)).to(torch.float)
    data.node_features = torch.from_numpy(np.array(node_features)).to(torch.float)
    #data.rhs = torch.from_numpy(np.array(feat_rhs)).to(torch.float)
    data.obj = torch.from_numpy(np.array(obj)).to(torch.float)
    data.edge_features = torch.from_numpy(np.array(edge_features)).to(torch.float)
    data.num_nodes = num_nodes
    data.demands = torch.from_numpy(demands).to(torch.long)
    data.num_demands = data.demands.size(0)
    data.num_edges_ = data.edge_features.size(0)
    data.var_nodes = torch.from_numpy(np.array(var_nodes)).to(torch.long)
    data.var_edges = torch.from_numpy(np.array(var_edges)).to(torch.long)
    
    return data, node_to_var



def list_processed_datasets():
    """Get a list of processed datasets (not used)
    """
    return list(os.listdir(os.path.expanduser("~/.cache/mipgnn/datasets/processed")))

# Preprocessing to create Torch dataset.
class GraphDataset(InMemoryDataset):
    """Graph dataset container, allows us to create array like datasets where items are graph objects. This class will automaticaly load all graph pickle files, apply the `create_data_object` function to each of them and store the resulting list of objects in the processed directory
    """

    def __init__(self, name, root, data_path, bias_threshold, transform=None, pre_transform=None,
                 pre_filter=None):

        self.bias_threshold = bias_threshold
        self.name = name
        self.data_path = data_path

        super(GraphDataset, self).__init__(root, transform, pre_transform, pre_filter)
        self.data, self.slices = torch.load(self.processed_paths[0])
        
    @property
    def raw_file_names(self):
        return self.name

    @property
    def processed_file_names(self):
        return self.name

    def download(self):
        pass

    def process(self):
        """Process graph files
        """
        print(f"Preprocessing files in {self.data_path}.")

        data_list = []
        
        graph_files = [f for f in os.listdir(self.data_path) if f.endswith("graph.pkl")]
        num_graphs = len(graph_files)

        with Bar(f"Processing {self.name}: ", max=num_graphs) as bar:
            # Iterate over instance files and create data objects.
            for num, filename in enumerate(graph_files):
                # Get graph.
                graph = None
                with open(os.path.join(self.data_path, filename), "rb") as f:
                    graph = pkl.load(f)
                #graph = nx.read_gpickle(os.path.join(self.data_path, filename))
                # get data object
                data,_ = create_data_object(graph, self.bias_threshold)
                data_list.append(data)
                bar.next()

        data, slices = self.collate(data_list)
        torch.save((data, slices), self.processed_paths[0])


# Preprocess indices of bipartite graphs to make batching work.
class MyData(Data):
    """Necessary for batching functionality
    """
    def __inc__(self, key, value, store):
        if key == "demands":
            return self.demands.size(0)
        else:
            return 0
    def __cat_dim__(self, key, value, *args, **kwargs):
        if key == "num_demands" or key=="num_edges_":
            return 0
        elif key == "y_real": 
            return None
        else: 
            return super().__cat_dim__(key, value, *args, **kwargs)

class MyTransform(object):
    def __call__(self, data):
        new_data = MyData()
        for key, item in data:
            new_data[key] = item
        return new_data


def load_dataset(dataset_path, bias_threshold=0.):
    """Utility function for loading a graph dataset from a folder
    """
    # its name is its basename
    dataset_name = os.path.basename(dataset_path)
    #processed_data_root = os.path.join(dataset_path, "processed")
    processed_data_root = dataset_path

    bias_threshold = 0.

    dataset = GraphDataset(dataset_name, processed_data_root, dataset_path, bias_threshold,
                                 transform=MyTransform()).shuffle()
    return dataset


if __name__=="__main__":
    if len(sys.argv)<2:
        print("A path was expected")
        exit()
    dataset_path = sys.argv[1]
    # its name is its basename
    dataset_name = os.path.basename(dataset_path)
    #processed_data_root = os.path.join(dataset_path, "processed")
    processed_data_root = dataset_path

    bias_threshold = 0.
    print(f"Processing dataset in {dataset_path}")
    print(f"\tname : {dataset_name}")
    print(f"\tprocessed data path : {processed_data_root}")

    dataset = GraphDataset(dataset_name, processed_data_root, dataset_path, bias_threshold,
                                 transform=MyTransform()).shuffle()
    print(f"\tN : ", len(dataset))



