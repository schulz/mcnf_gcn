import numpy as np
import networkx as nx

from mcnf import MCNF

# create a simple MCNF instance
G = nx.MultiDiGraph()
G.add_edge(1,2,cost=1,capacity=100)
G.add_edge(1,4,cost=1,capacity=100)
G.add_edge(2,3,cost=1,capacity=100)
G.add_edge(4,3,cost=1,capacity=100)

demands = np.array([[1,3,150]])

instance = MCNF(G, demands)

cpx_status, cpx_solution, cpx_solve_t = instance.solve_with_cplex()

print("CPLEX solver status : ", cpx_status)
print("CPLEX solve time : ", cpx_solve_t)
print("CPLEX solution : ", cpx_solution)

# reconstruct the paths used to route the demands
cpx_paths, cpx_flows = instance.solution_paths(cpx_solution)
for k,demand in enumerate(demands):
    print(f"Demand {k}: ", demand)
    for path,flow in zip(cpx_paths[k], cpx_flows[k]):
        print("\t",path, ", flow: ", flow)

# tester 
