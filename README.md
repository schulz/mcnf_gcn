# Huawei project
We are trying to improve the performance of an optimization solver for Multi-Commodity Network Flow problems. A graph and a collection
of demands have been given to us. Every problem is defined by K instances

## Installation
This module requires a working version of CPLEX as well as the corresponding python `cplex` package. `cplex`'s installation script is usually found under `/opt/ibm/ILOG/CPLEX_Studio/python`.

Create a virtual environment and install dependencies (use `requirements_dev.txt` if encountering conflicts on installation).

```
python3 -m venv venv
. venv/bin/activate
python -m pip install -r requirements.txt
```

## Python
For the following make sure that you are under the `python` directory.

### Dataset generation
Dataset generation functions are implemented in the `dataset_generator.py` file. Datasets are stored on the hard drive as a collection 
of `.mps`, `.sol` and `_graph.pkl` files respectively containing the MIP problem instance, the solution computed with CPLEX and a
pickled `networkx` graph with node and edge features as well as the labels.

Execute the following to generate a dataset of `n` instances with `n_demands` demands, files are saved in `output_directory`.

```
python dataset_generator.py <#_instances> <#_demands> <output_directory>
```

## Model training
Training a model is done in the `train_model.py` file. Train a model with the `train_model.py` script : 

```
python3 train_model.py <train_dataset_path> [OPTIONAL ARGS]
```

A `metadata.json` file containing training parameters will be created along with two plots.

Optional arguments include :
- `-b` or `--batch-size` : batch size (not fully functional)
- `-e` or `--epochs` : number of epochs
- `-l` or `--layers` : number of message passing layers
- `-r` or `--lr` : learning rate
- `-p` or `--patience` : early stopping patience
- `-d` or `--hidden` : dimension of hidden layer
- `-o` or `--output-directory` : training output directory
- `--checkpoint` : path to previously saved checkpoint, load model from checkpoint


    
